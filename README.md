# Form Digital Assessment

### Goals of this project
The goals of this project are as follows:
Activity designed to test your knowledge of React.js technology and assess your ability to
create Web applications that handle User information with a UI based on reusable components
provided by a Design System

### Requirements
- Create a React.js based application that allows the user to
- Encode their name and email into a JWT token using the RS256 algorithm (you may
use the supplied keys, or generate your own)
- See the encoded token result
- Verify that the JWT token signature is valid
- Load a private image (mocking the request is enough)
- Use the JWT as HTTP Authentication header when loading an image resource (i.e.
https://picsum.photos/200/300?random=1)
- Use Reusable UI Components to render all buttons and input fields (name, email, private
key, public key)
- Use Reusable UI Components to render all outputs (output token, verification boolean)

### Technologies used
- ReactJS
- Prettier
- Formik
- React Router DOM
- Carbon React by IBM
- TailwindCSS
- PostCSS
- NodeJS
- ExpressJS
- jsonwebtoken
- body-parser

### Applcation
The application uses a client-server approach with a node server mounting routes to `/api`
and serving static content from the `client/build` folder. 

### Development
You can run both the server and client app using the following command
```sh
yarn dev
```
This will run the API at `http://localhost:3001` and client app at `http://localhost:3000`.
Request in the client app are proxied over to `:3001`. 

You can alternative run each project individually using the following
API:
```sh
yarn server
```

Client:
```sh
cd client && yarn start
```

### Production
In production, the Nodejs server will serve static files that are produced by running `yarn build` from within the `client` folder. 
You may also use `ts-node` to run the `main.ts` file,
but it is recommeneded to compile the typescript files using `yarn build` form the project root and deploy the resulting files from
the `dist` folder.

#### Mentionable folders

###### client/src/primitives
The primitives folder contains very bare building blocks that
can be reused as presentational components

###### client/src/components
The components folder contains components that have much more logic attached to them. This allows
us to separate our presentational components from those that do much more, like modifying state.

### State management
The application does not store much global state. Just a `token` and an `email`. I decided to use
React's Context API in order to avoid bringing in any unnecessary third party dependencies. Keep state management clean
and within React's paradigm. The related context files may be found under `client/src/auth`. 

### Token signing and verification
Token signing and verification occurs on two API routes
- `api/auth/token` retrieves a token when provided an email and name
- `api/auth/token/verify` verifies a given token for a given email

Token verification and signing is done with Auth0's [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)
using the `RS256` algorithm. 
