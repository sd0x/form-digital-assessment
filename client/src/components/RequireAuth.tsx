import React, { useEffect } from "react";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { removeToken, useAuth } from "../auth";

export function RequireAuth({ children }: { children: JSX.Element }) {
  let { state, dispatch } = useAuth();
  let location = useLocation();
  let navigate = useNavigate();

  useEffect(() => {
    if (!state.token || !state.email) {
      navigate("/", { replace: true });
      return;
    }

    (async () => {
      try {
        const response = await fetch("api/auth/token/verify", {
          method: "POST",
          body: JSON.stringify({
            email: state.email, 
          }),
          headers: {
            Authorization: state.token!,
            "Content-Type": "application/json",
          }
        });

        if (response.status !== 200) {
          dispatch(removeToken());
          throw new Error("Unable to verify token")
        }
      } catch(e) {
        dispatch(removeToken());
        navigate("/", { replace: true });
      }
    })();
  }, [state, dispatch, navigate])

  if (!state.token) {
    // Redirect to index login if no token or it is invalid
    return <Navigate to="/" state={{ from: location }} replace />;
  }


  return children;
}