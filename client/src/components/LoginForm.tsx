import React from "react";
import { Button, ErrorText, PageContainer } from "../primitives";
import { FormikHelpers, useFormik } from "formik";
import { Input } from "../primitives";

interface FormValues {
  name: string;
  email: string;
}

interface LoginFormProps {
  name?: string;
  email?: string;
  error?: string;
  onSubmit: (values: FormValues) => Promise<void>;
}

const LoginForm: React.FC<LoginFormProps> = ({ name, email, error, onSubmit }) => {
  const _onSubmit = async (
    values: FormValues,
    actions: FormikHelpers<FormValues>
  ) => {
    if (!values) {
      actions.setErrors({
        name: "Name is required",
        email: "Email is required",
      });
      return;
    }

    if (!values.name) {
      actions.setErrors({ name: "Name is required" });
      return;
    }

    if (!values.email) {
      actions.setErrors({ email: "Email is required" });
      return;
    }

    await onSubmit(values);
  };

  const {
    isSubmitting,
    errors,
    touched,
    handleSubmit,
    handleChange,
    handleBlur,
    values
  } = useFormik({
    initialValues: {
      email: email ?? "",
      name: name ?? "",
    },
    onSubmit: _onSubmit,
  });

  return (
    <PageContainer className="justify-center items-center flex-1">
      <div className="w-full md:w-72">
        <form className="flex flex-col space-y-4">
          <Input
            type="text"
            name="name"
            placeholder="Name"
            onInput={handleChange}
            onBlur={handleBlur}
            value={values.name}
          />
          {errors.name && touched.name && errors.name && (
            <ErrorText>{errors.name}</ErrorText>
          )}
          <Input
            type="email"
            name="email"
            placeholder="Email"
            onInput={handleChange}
            onBlur={handleBlur}
            value={values.email}
          />
          {errors.email && touched.email && errors.email && (
            <ErrorText>{errors.email}</ErrorText>
          )}
          <Button
            type="submit"
            disabled={isSubmitting}
            onClick={() => handleSubmit()}
          >
            Submit
          </Button>
          {error && (
            <ErrorText>{error}</ErrorText>
          )}
        </form>
      </div>
    </PageContainer>
  );
};

export default LoginForm;
