import React, { useEffect, useState } from "react";
import { removeToken, useAuth } from "../auth";
import { Button, PageContainer, ErrorText, Spinner } from "../primitives";

const DashboardPage: React.FC = () => {
  const { state, dispatch } = useAuth();
  const [resource, setResource] = useState<string>();
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);
  const onLogout = () => {
    dispatch(removeToken());
  };

  useEffect(() => {
    if (!state.token) {
      return;
    }

    setLoading(true);
    (async () => {
      try {
        const response = await fetch("https://picsum.photos/200/300?random=1", {
          headers: {
            Authorization: `Bearer ${state.token}`,
          },
        });

        if (response.status !== 200) {
          throw new Error("Unable to retrieve resource");
        }

        const data = await response.blob();
        setResource(URL.createObjectURL(data));
      } catch (e: any) {
        setError(e.toString());
      } finally {
        setLoading(false);
      }
    })();
  }, [state.token]);

  return (
    <PageContainer className="space-y-4">
      {resource && <img className="w-1/3" src={resource} alt="Resource" />}
      {loading && (
        <div className="h-12 w-12 flex flex-col justify-center items-center">
          <Spinner />
        </div>
      )}
      {error && (
        <ErrorText>Unable to retrieve resource</ErrorText>
      )}
      <Button onClick={onLogout}>Logout</Button>
    </PageContainer>
  );
};

export default DashboardPage;
