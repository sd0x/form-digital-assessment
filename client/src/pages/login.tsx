import React, { useState } from "react";
import { PageContainer } from "../primitives";
import { addToken, removeToken, useAuth } from "../auth";
import { useNavigate } from "react-router-dom";
import LoginForm from "../components/LoginForm";


const LoginPage: React.FC = () => {
  const { dispatch } = useAuth();
  let navigate = useNavigate();
  const [error, setError] = useState<string>();

  const onSubmit = async (values: { email: string; name: string; }) => {
    try {
      const response = await fetch("api/auth/token", {
        method: "POST",
        body: JSON.stringify({
          email: values.email,
          name: values.name,
        }),
        headers: {
          "Content-Type": "application/json",
        }
      });

      if (response.status !== 200) {
        throw new Error("an error was encountered");
      }

      const data = await response.json();

      if (!data.token) {
        throw new Error("unable to retrive token");
      }

      dispatch(addToken(data.token, values.email));
      navigate("/dashboard", { replace: true});
    } catch (e: any) {
      dispatch(removeToken());
      setError("Unable to retrieve token");
    }
  }

  return (
    <PageContainer className="justify-center items-center flex-1">
      <div className="w-full md:w-72">
        <LoginForm
          error={error}
          onSubmit={onSubmit}
        />
      </div>
    </PageContainer>
  );
};

export default LoginPage;
