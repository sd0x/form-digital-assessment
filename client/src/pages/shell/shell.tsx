import React, { HTMLAttributes, useMemo } from "react";
import cx from "classnames";
import "./shell.css";
import { Outlet } from "react-router-dom";
import { Link } from "react-router-dom";
import { Link as CarbonLink, Header } from "../../primitives";
import { useAuth } from "../../auth";

interface ShellProps extends HTMLAttributes<HTMLDivElement> {}

export const Shell: React.FC<ShellProps> = ({
  className,
  children,
  ...props
}) => {
  const { state } = useAuth();

  const isLoggedIn = useMemo(() => {
    return !!state.token;
  }, [state]);

  return (
    <>
      <Header>
        <div>Assessment</div>
        <div aria-label="navigation">
          {!isLoggedIn && (
            <Link to="/">
              <CarbonLink>Login</CarbonLink>
            </Link>
          )}
          {isLoggedIn && (
            <Link to="/dashboard">
              <CarbonLink>Dashboard</CarbonLink>
            </Link>
          )}
        </div>
      </Header>
      <div className={cx("bx--grid shell flex-1", className)} {...props}>
        <main className="flex flex-col h-full">
          <Outlet />
        </main>
      </div>
    </>
  );
};
