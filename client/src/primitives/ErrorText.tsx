import React from "react";
import cx from "classnames";

export const ErrorText: React.FC<React.HTMLAttributes<HTMLSpanElement>> = ({
  children,
  className,
  ...props
}) => (
  <span className={cx("", className)} {...props}>
    {children}
  </span>
);
