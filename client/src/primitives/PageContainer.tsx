import React from "react";
import cx from "classnames";

export const PageContainer: React.FC<React.HTMLAttributes<HTMLDivElement>> = ({
  className,
  children,
  ...props
}) => {
  return (
    <div className={cx("flex-1 flex flex-col px-8 py-8", className)} {...props}>
      {children}
    </div>
  );
};
