import React from "react";

export const Header: React.FC<React.HTMLAttributes<HTMLElement>> = ({
  children,
  ...props
}) => {
  return (
    <header
      aria-label="FD Assessment"
      className="flex flex-row space-x-4 mb-4 bg-gray-900 px-6 py-4 text-white"
      {...props}
    >
      {children}
    </header>
  );
};
