import React from "react";
import { Button } from "carbon-components-react";
import {
  AppSwitcher20,
  Notification20,
  UserAvatar20,
} from "@carbon/icons-react";
import { Shell } from "./pages/shell";
import "./app.css";

import { RequireAuth } from "./components";
import { Routes, Route } from "react-router-dom";
import LoginPage from "./pages/login";
import DashboardPage from "./pages/dashboard";
import { AuthProvider } from "./auth";

function App() {
  return (
    <div className="app">
      <AuthProvider>
        <Routes>
          <Route path="/" element={<Shell />}>
            <Route index element={<LoginPage />} />
            <Route
              path="/dashboard"
              element={
                <RequireAuth>
                  <DashboardPage />
                </RequireAuth>
              }
            />
          </Route>
        </Routes>
      </AuthProvider>
    </div>
  );
}

export default App;
