import React, { Dispatch, useContext } from "react";
import { Action } from "./reducer";


export interface AuthState {
  token: string | null,
  email: string | null,
  valid: boolean;
}

export interface IAuthContext {
  state: AuthState;
  dispatch: Dispatch<Action>;
}

export const initialState = {
    token: null,
    email: null,
    valid: false,
};

export const AuthContext = React.createContext<IAuthContext>({
  state: initialState,
  dispatch: (action: Action) => initialState,
});

export function useAuth() {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth must be used within an AuthProvider");
  }

  return context;
}
