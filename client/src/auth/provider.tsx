import React, { useReducer } from "react";
import { AuthContext, initialState } from "./context";
import { tokenReducer } from "./reducer";

export const AuthProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(tokenReducer, initialState);
  const value = {
    state,
    dispatch,
  };

  return (
    <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
  );
} 