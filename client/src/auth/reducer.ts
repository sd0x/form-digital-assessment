import { AuthState } from "./context";

const ADD_TOKEN = "ADD_TOKEN";
const REMOVE_TOKEN = "REMOVE_TOKEN";

export type Action =
 | { type: typeof ADD_TOKEN, token: string, email: string }
 | { type: typeof REMOVE_TOKEN };

export function tokenReducer(state: AuthState, action: Action): AuthState {
  switch(action.type) {
    case ADD_TOKEN:
      return { ...state, token: action.token, email: action.email };
    case REMOVE_TOKEN:
      return { ...state, token: null, valid: false, email: null };
    default:
      return state;
  }
}

export const addToken = (token: string, email: string): Action => {
  return {
    type: ADD_TOKEN,
    token,
    email
  };
}

export const removeToken = (): Action => {
  return {
    type: REMOVE_TOKEN,
  };
}
