import bodyParser from "body-parser";
import express from "express";
import dotenv from "dotenv";
// handlers
import getToken from "./get-token";
import verifyToken from "./verify-token";

const app = express();
const port = 3001;

app.set("port", process.env.PORT || port);

app.use(
  bodyParser.json({
    inflate: true,
    limit: 1000,
  })
);

// serve static webapp
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
}

app.post("/api/auth/token", getToken);

app.post("/api/auth/token/verify", verifyToken);

app.listen(app.get("port"), function () {
  console.log(`api listening on 127.0.0.1:${app.get("port")}`);
});
