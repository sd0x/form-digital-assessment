import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import fs from "fs";
import { Issuer, Audience, expiry } from "./config";

const publicKey = fs.readFileSync(__dirname + "/public.pem", "utf8");

export default function (req: Request, res: Response) {
  if (!req.headers.authorization) {
    res.status(401);
    res.send("Unauthorized");
    return;
  }

  if (!req.body.email) {
    res.status(400);
    res.send("Bad Request: email is required");
    return;
  }

  const token = req.headers.authorization;
  const { email } = req.body;

  try {
    jwt.verify(token, publicKey, {
      issuer: Issuer,
      audience: Audience,
      subject: email,
      maxAge: expiry,
      algorithms: ["RS256"],
    });

    res.status(200);
    res.send();
  } catch(e) {
    res.status(401);
    res.send("Unauthorized")
  }
}
