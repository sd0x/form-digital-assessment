export const Audience = process.env.AUDIENCE || "http://localhost:3000";
export const Issuer = process.env.ISSUER || "AssessmentApi";

export const expiry = "24h";
