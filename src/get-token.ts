import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import fs from "fs";
import { Issuer, Audience, expiry } from "./config";

const privateKey = fs.readFileSync(__dirname + "/private.pem", "utf8");

export default function (req: Request, res: Response) {
  if (!req.body.email) {
    res.status(400);
    res.send("Bad request: email is required");
    return;
  }

  if (!req.body.name) {
    res.status(400);
    res.send("Bad request: name is required");
    return;
  }
  
  const { email, name } = req.body;
  const payload = {
    email,
    name,
  };

  const token = jwt.sign(payload, privateKey, {
    algorithm: "RS256",
    expiresIn: expiry,
    issuer: Issuer,
    subject: email,
    audience: Audience,
  });

  res.json({
    token,
  });
}
